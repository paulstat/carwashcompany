package com.forgerock.carwashcompany.minion;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import com.forgerock.carwashcompany.customer.CarWashCustomer;
import com.forgerock.carwashcompany.customer.Edith;
import com.forgerock.carwashcompany.customer.Vector;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;

public class TestMinion {

    @Test
    public void testRun() throws Exception {
        BlockingQueue<CarWashCustomer> queue = new ArrayBlockingQueue<>(2);
        CarWashCustomer edith = new Edith();
        CarWashCustomer vector = new Vector();
        queue.add(edith);
        queue.add(vector);

        Logger logger = (Logger) LoggerFactory.getLogger(Minion.class);
        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();

        logger.addAppender(listAppender);

        Minion minion = new Bob(queue);

        Thread t = new Thread(minion);
        t.start();
        t.join();

        List<ILoggingEvent> logsList = listAppender.list;

        ILoggingEvent greetingEvent = logsList.get(0);
        
        assertEquals("Minion Bob ready for action!", greetingEvent.getMessage());
        assertEquals(Level.INFO, greetingEvent.getLevel());
        
        ILoggingEvent dieEvent = logsList.get(logsList.size() - 1);
        
        assertEquals("Bob: ARGH!!! BLEURGH {DIES}", dieEvent.getMessage());
        assertEquals(Level.INFO, dieEvent.getLevel());
    }

}
