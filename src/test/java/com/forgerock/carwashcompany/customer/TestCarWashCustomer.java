package com.forgerock.carwashcompany.customer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;

public class TestCarWashCustomer {
    
    static class TestCustomer extends CarWashCustomer {
        
        public TestCustomer() {
            super("Paul", Arrays.asList("Hi"));
        }
        
    }
    
    @Test
    public void testGreetMinion() {
        Logger logger = (Logger) LoggerFactory.getLogger(CarWashCustomer.class);
        
        ListAppender<ILoggingEvent> listAppender = new ListAppender<>();
        listAppender.start();

        logger.addAppender(listAppender);
        
        CarWashCustomer testCustomer = new TestCustomer();
        testCustomer.greetMinion();
        
        List<ILoggingEvent> logsList = listAppender.list;
        ILoggingEvent greetingEvent = logsList.get(0);
        
        assertEquals("Paul: Hi", greetingEvent.getMessage());
        assertEquals(Level.INFO, greetingEvent.getLevel());
    }

}
