package com.forgerock.carwashcompany;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class TestCompanyManagerFactory {
    
    @Test
    public void testCreateInstance() {
        CompanyManager manager = CompanyManagerFactory.createInstance();
        
        assertTrue(manager instanceof ParallelCompanyManager);
        
        System.setProperty(CompanyManagerFactory.SEQUENTIAL_MODE, "true");
        
        manager = CompanyManagerFactory.createInstance();
        
        assertTrue(manager instanceof SequentialCompanyManager);
    }

}
