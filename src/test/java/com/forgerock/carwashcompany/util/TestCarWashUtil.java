package com.forgerock.carwashcompany.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.Test;

public class TestCarWashUtil {

    @Test
    public void testPickRandomQuote() {
        List<String> quotes = Arrays.asList("A", "B", "C");
        
        String randomQuote = CarWashUtil.pickRandomQuote(quotes);
        
        assertNotNull(randomQuote);
        assertFalse(randomQuote.isEmpty());
        assertTrue(quotes.contains(randomQuote));
        
        IllegalArgumentException e = assertThrows(IllegalArgumentException.class,
                () -> CarWashUtil.pickRandomQuote(Collections.emptyList()));
        
        assertEquals("String list cannot be null or empty!", e.getMessage());
        
        e = assertThrows(IllegalArgumentException.class,
                () -> CarWashUtil.pickRandomQuote(null));
        
        assertEquals("String list cannot be null or empty!", e.getMessage());
    }

}
