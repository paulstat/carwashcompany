package com.forgerock.carwashcompany.customer;

import java.util.Arrays;
import java.util.List;

public class Agnes extends CarWashCustomer {

    private static final List<String> QUOTES = Arrays.asList("It's so fluffy I'm gonna die.",
            "Why are your wearing pyjamas", "Aw, my caterpillar didn't turn into a butterfly");
    private static final String NAME = "Agnes";

    public Agnes() {
        super(NAME, QUOTES);
    }

}
