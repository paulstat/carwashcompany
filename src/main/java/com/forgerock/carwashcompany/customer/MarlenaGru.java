package com.forgerock.carwashcompany.customer;

import java.util.Arrays;
import java.util.List;

public class MarlenaGru extends CarWashCustomer {

    private static final List<String> QUOTES = Arrays
            .asList("I'm afraid you are too late, son. NASA isn't sending the monkeys anymore.");
    private static final String NAME = "Marlena Gru";

    public MarlenaGru() {
        super(NAME, QUOTES);
    }

}
