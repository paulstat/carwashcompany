package com.forgerock.carwashcompany.customer;

import java.util.Arrays;
import java.util.List;

public class Margo extends CarWashCustomer {

    private static final List<String> QUOTES = Arrays.asList("Just so you know you're never going to be my dad!");
    private static final String NAME = "Margo";
    
    public Margo() {
        super(NAME, QUOTES);
    }
    
}
