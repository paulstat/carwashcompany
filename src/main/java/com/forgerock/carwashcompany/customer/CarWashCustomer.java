package com.forgerock.carwashcompany.customer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.forgerock.carwashcompany.util.CarWashUtil;

/**
 * 
 * @author Paul Statham
 *
 */
public abstract class CarWashCustomer {
    
    private static final Logger logger = LoggerFactory.getLogger(CarWashCustomer.class);
    
    private final String name;
    private final List<String> quotes;
    
    public CarWashCustomer(String name, List<String> quotes) {
        this.name = name;
        this.quotes = quotes;
    }
    
    public void greetMinion() {
        String quote = CarWashUtil.pickRandomQuote(quotes);
        logger.info(name + ": " + quote);
    }
    
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
    

}
