package com.forgerock.carwashcompany.customer;

import java.util.Arrays;
import java.util.List;

public class DrNefario extends CarWashCustomer {
    
    private static final List<String> QUOTES = Arrays.asList("Do you want to explode?", "A dozen boogie robots. Boogie! Look at this! Watch me.");
    private static final String NAME = "Dr Nefario";

    public DrNefario() {
        super(NAME, QUOTES);
    }

}
