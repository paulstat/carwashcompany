package com.forgerock.carwashcompany.customer;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author Paul Statham
 *
 */
public class FelaniousGru extends CarWashCustomer {

    private static final List<String> QUOTES = Arrays.asList("Hey mom! I build a rocket based on the macarroni model!",
            "I said DART gun", "I'm going to need a dozen robots disguised as cookies", "Assemble the minions!",
            "This is very important, you have to get the little girl a new unicorn toy.");
    private static final String NAME = "Felanious Gru";

    public FelaniousGru() {
        super(NAME, QUOTES);
    }

}
