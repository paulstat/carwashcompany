package com.forgerock.carwashcompany.customer;

import java.util.Arrays;
import java.util.List;

/**
 * Vector is an evil Villain that will poison any minion that tries to clean his car!
 * 
 * @author Paul Statham
 *
 */
public class Vector extends CarWashCustomer {

    private static final String NAME = "Vector";
    
    private static final String QUOTE = "Hahaha I go by the name of Vector. "
            + "It's a mathematical term, represented by an arrow with both direction "
            + "and magnitude. Vector! That's me, because I commit crimes with both direction"
            + " and magnitude. Oh yeah! And now I will KILL YOU!";
    
    private static final List<String> QUOTES = Arrays.asList(QUOTE);
    
    public Vector() {
        super(NAME, QUOTES);
    }

}
