package com.forgerock.carwashcompany.customer;

import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author Paul Statham
 *
 */
public class Edith extends CarWashCustomer {
    
    private static final List<String> QUOTES = Arrays.asList("Can I drink this?", "Hey! It's so dark in here.", "It Poke a hole in my juice box");
    private static final String NAME = "Edith";

    public Edith() {
        super(NAME, QUOTES);
    }

}
