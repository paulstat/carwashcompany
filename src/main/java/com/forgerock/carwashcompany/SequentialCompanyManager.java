package com.forgerock.carwashcompany;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.SynchronousQueue;

import com.forgerock.carwashcompany.customer.CarWashCustomer;

/**
 * Sequential Company manager relies on Bob to perform tasks in sequence one after the other
 * 
 * 
 * @author Paul Statham
 *
 */
public class SequentialCompanyManager extends CompanyManager {
    
    @Override
    protected BlockingQueue<CarWashCustomer> createCarWashQueue() {
        return new SynchronousQueue<>();
    }

}
