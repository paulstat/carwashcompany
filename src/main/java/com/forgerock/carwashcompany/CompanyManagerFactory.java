package com.forgerock.carwashcompany;

/**
 * Simple Factory for creating an instance of a CompanyManager
 * 
 * @author Paul Statham
 *
 */
public final class CompanyManagerFactory {
    
    static final String SEQUENTIAL_MODE = "sequential";
    
    /**
     * Creates an instance of Sequential/Parallel CompanyManager
     * depending on the presence of the sequential system property.
     * 
     * @return
     */
    public static CompanyManager createInstance() {
        if (System.getProperty(SEQUENTIAL_MODE) != null) {
            return new SequentialCompanyManager();
        } else {
            return new ParallelCompanyManager();
        }
    }

}
