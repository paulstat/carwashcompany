package com.forgerock.carwashcompany;

/**
 * Hello world!
 *
 */
public class App {
        
    public static void main(String[] args) {
        CompanyManager manager = CompanyManagerFactory.createInstance();
        
        manager.startServing();
    }
    
}
