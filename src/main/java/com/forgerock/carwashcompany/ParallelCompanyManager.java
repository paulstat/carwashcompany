package com.forgerock.carwashcompany;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.forgerock.carwashcompany.customer.CarWashCustomer;

/**
 * Parallel Company manager employs a team of minions to work on tasks in parallel
 * 
 * 
 * @author Paul Statham
 *
 */
public class ParallelCompanyManager extends CompanyManager {
    
    private static final int BOUNDED_QUEUE_SIZE = 10;
    
    @Override
    protected BlockingQueue<CarWashCustomer> createCarWashQueue() {
        return new ArrayBlockingQueue<>(BOUNDED_QUEUE_SIZE);
    }


}
