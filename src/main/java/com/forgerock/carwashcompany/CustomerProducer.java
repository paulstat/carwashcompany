package com.forgerock.carwashcompany;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.forgerock.carwashcompany.customer.Agnes;
import com.forgerock.carwashcompany.customer.CarWashCustomer;
import com.forgerock.carwashcompany.customer.DrNefario;
import com.forgerock.carwashcompany.customer.Edith;
import com.forgerock.carwashcompany.customer.FelaniousGru;
import com.forgerock.carwashcompany.customer.Margo;
import com.forgerock.carwashcompany.customer.MarlenaGru;
import com.forgerock.carwashcompany.customer.Vector;

/**
 * Producer class which attempts to seed a blocking queue with random instances
 * of CarWashCustomer every 2 seconds
 * 
 * @author Paul Statham
 *
 */
public class CustomerProducer implements Runnable {

    private static final long PRODUCE_RATE = 2000;
    
    private static final Logger logger = LoggerFactory.getLogger(CustomerProducer.class);
    
    private static final List<Class<? extends CarWashCustomer>> CUSTOMER_CLASSES = Arrays.asList(Agnes.class,
            DrNefario.class, Edith.class, FelaniousGru.class, Margo.class, MarlenaGru.class, Vector.class);
    
    private final BlockingQueue<CarWashCustomer> carWashQueue;
    
    /**
     * Create a new instance of the producer
     * 
     * @param carWashQueue - queue to seed with customers
     */
    public CustomerProducer(BlockingQueue<CarWashCustomer> carWashQueue) {
        this.carWashQueue = carWashQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                final int startIndex = 0;
                final int endIndex = CUSTOMER_CLASSES.size();
                
                final int randIndex = ThreadLocalRandom.current().nextInt(startIndex, endIndex);
                
                CarWashCustomer customer = CUSTOMER_CLASSES.get(randIndex).newInstance();
                
                logger.info("Customer {} arrived. Welcome! Please join the queue.", customer);
                carWashQueue.put(customer);
                logger.info("Customer {} has joined the queue.", customer);
                
                Thread.sleep(PRODUCE_RATE);
            } catch (InterruptedException | InstantiationException | IllegalAccessException e) {
                logger.error("Uhoh! No more customers today!", e);
            }
        }
    }

}
