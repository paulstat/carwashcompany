package com.forgerock.carwashcompany.minion;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import com.forgerock.carwashcompany.customer.CarWashCustomer;

public class Bob extends Minion {
    
    private static final List<String> QUOTES = Arrays.asList("Splosh", "Scrub", "Banana!", "King Bob!", "Villain Con!");
    
    private static final String NAME = "Bob";
    
    public Bob(BlockingQueue<CarWashCustomer> carWashQueue) {
        super(NAME, QUOTES, carWashQueue);
    }

}
