package com.forgerock.carwashcompany.minion;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

import com.forgerock.carwashcompany.customer.CarWashCustomer;

/**
 * 
 * @author Paul Statham
 *
 */
public class MinionFactory {

    private static final List<Class<? extends Minion>> MINION_CLASSES = Arrays.asList(Bob.class, Dave.class,
            Jerry.class, Phil.class, Stuart.class, Tim.class);

    /**
     * Creates a complete workforce ready for action!
     * 
     * @param carWashQueue
     * @return
     */
    public static List<Minion> createWorkForce(BlockingQueue<CarWashCustomer> carWashQueue) {
        return Arrays.asList(new Bob(carWashQueue), new Dave(carWashQueue), new Jerry(carWashQueue),
                new Phil(carWashQueue), new Stuart(carWashQueue), new Tim(carWashQueue));
    }

    /**
     * Creates a random instance of a minion from the known minions
     * 
     * @param carWashQueue
     * @return
     */
    public static Minion createRandomMinion(BlockingQueue<CarWashCustomer> carWashQueue) {
        try {
            int randIndex = ThreadLocalRandom.current().nextInt(0, MINION_CLASSES.size());
            Class<? extends Minion> minionClass = MINION_CLASSES.get(randIndex);
            
            Constructor<? extends Minion> constructor = minionClass.getConstructor(BlockingQueue.class);

            return constructor.newInstance(carWashQueue);
        } catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
                | IllegalArgumentException | InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return null;
    }

}
