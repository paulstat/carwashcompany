package com.forgerock.carwashcompany.minion;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadLocalRandom;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.forgerock.carwashcompany.customer.CarWashCustomer;
import com.forgerock.carwashcompany.customer.Vector;
import com.forgerock.carwashcompany.util.CarWashUtil;

/**
 * An abstract Minion (worker) which washes cars
 * 
 * @author Paul Statham
 *
 */
public abstract class Minion implements Runnable {

    private static final long SLEEP_PERIOD = 2000;
    
    private static final String BOOTSTRAP_GREETING = "ready for action!";
    private static final String MINION = "Minion";
    private static final String GREETING = "Ello I wash you now";
    private static final String DIE = "ARGH!!! BLEURGH {DIES}";

    private static final int MIN_LOOPS = 1;
    private static final int MAX_LOOPS = 10;

    static final Logger logger = LoggerFactory.getLogger(Minion.class);

    private BlockingQueue<CarWashCustomer> carWashQueue;

    private final String name;
    private final List<String> quotes;

    /**
     * Creates an instance of a minion
     * 
     * @param name         - name of the minion
     * @param quotes       - list of quotes for the minion
     * @param carWashQueue - queue of customers
     */
    public Minion(String name, List<String> quotes, BlockingQueue<CarWashCustomer> carWashQueue) {
        this.name = name;
        this.quotes = quotes;
        this.carWashQueue = carWashQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                logger.info(this + " " + BOOTSTRAP_GREETING);
                while (true) {
                    CarWashCustomer customer = carWashQueue.take();

                    customer.greetMinion();

                    // Vector will poison the minion!
                    if (customer instanceof Vector) {
                        logger.info(name + ": " + DIE);
                        return;
                    }

                    logger.info(name + ": " + GREETING);

                    wash();

                    logger.info(name + ": OK " + customer.getName() + ", you done now, bye bye!");
                }
            } catch (InterruptedException e) {
                logger.error(DIE, e);
            }
        }

    }

    /**
     * Let's get washing!
     * 
     * @throws InterruptedException
     */
    private void wash() throws InterruptedException {
        for (int loopCount = ThreadLocalRandom.current().nextInt(MIN_LOOPS, MAX_LOOPS); loopCount > 0; loopCount--) {
            String quote = CarWashUtil.pickRandomQuote(quotes);
            logger.info("{}: {}", name, quote);
            Thread.sleep(SLEEP_PERIOD);
        }

    }

    /**
     * String representation of this minion eg.
     * 
     * "Minion Tim"
     * 
     */
    @Override
    public String toString() {
        return MINION + " " + name;
    }

}
