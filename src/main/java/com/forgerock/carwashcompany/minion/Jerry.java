package com.forgerock.carwashcompany.minion;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import com.forgerock.carwashcompany.customer.CarWashCustomer;

public class Jerry extends Minion {
    
    private static final List<String> QUOTES = Arrays.asList("Splosh", "Scrub", "Banana!");
    
    private static final String NAME = "Jerry";
    
    public Jerry(BlockingQueue<CarWashCustomer> carWashQueue) {
        super(NAME, QUOTES, carWashQueue);
    }

}
