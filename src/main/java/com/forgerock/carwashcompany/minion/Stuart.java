package com.forgerock.carwashcompany.minion;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;

import com.forgerock.carwashcompany.customer.CarWashCustomer;

public class Stuart extends Minion {

    private static final List<String> QUOTES = Arrays.asList("Mi bellas!",
            "No, No, No, Kevin, let me do it, let me do it, Spita", "Splosh", "Scrub", "Banana!");

    private static final String NAME = "Stuart";

    public Stuart(BlockingQueue<CarWashCustomer> carWashQueue) {
        super(NAME, QUOTES, carWashQueue);
    }

}
