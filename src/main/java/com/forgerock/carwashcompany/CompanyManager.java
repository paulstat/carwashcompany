package com.forgerock.carwashcompany;

import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.forgerock.carwashcompany.customer.CarWashCustomer;
import com.forgerock.carwashcompany.minion.Minion;
import com.forgerock.carwashcompany.minion.MinionFactory;

/**
 * Company manager responsible for instantiating the queue and manager the pool
 * of minions!
 * 
 * @author Paul Statham
 *
 */
public abstract class CompanyManager {
    
    private static final Logger logger = LoggerFactory.getLogger(CompanyManager.class);
    private static final long MONITOR_SLEEP = 5000;
    
    private static final int POOL_SIZE = 6;

    private BlockingQueue<CarWashCustomer> carWashQueue = createCarWashQueue();
    private CustomerProducer customerProducer = new CustomerProducer(carWashQueue);
    private ExecutorService producerPool = Executors.newSingleThreadExecutor();
    
    private ExecutorService minionPool;
    
    private List<Minion> minions;

    public CompanyManager() {
        initMinionWorkForce();
    }

    
    /**
     * Creates an implementation specific BlockingQueue
     * 
     * @return
     */
    protected abstract BlockingQueue<CarWashCustomer> createCarWashQueue();
    
    private void initMinionWorkForce() {
        logger.info("Initialising Minions!");

        minionPool = Executors.newFixedThreadPool(POOL_SIZE);
        minions = MinionFactory.createWorkForce(carWashQueue);
        
        minions.forEach(minion -> minionPool.submit(minion));
    }
    
    /**
     * Start a new day serving despicable customers!
     */
    public void startServing() {
        producerPool.submit(customerProducer);

        while (true) {
            monitorActivity();
        }

    }
    
    /**
     * Monitors the active thread count within the minion pool
     * if Vector has killed some minions we need to respawn!
     */
    private void monitorActivity() {
        try {
            long activeThreads = ((ThreadPoolExecutor) minionPool).getActiveCount();
            
            if (activeThreads < minions.size()) {
                logger.info("Hmmm our workforce is depleted, respawn!");
                
                while (activeThreads < minions.size()) {
                    minionPool.submit(MinionFactory.createRandomMinion(carWashQueue));
                    
                    activeThreads = ((ThreadPoolExecutor) minionPool).getActiveCount();
                }
                
            }
            
            Thread.sleep(MONITOR_SLEEP);
        } catch (InterruptedException e) {
            logger.warn("Uhoh interrupted whilst monitoring", e);
        }
    }
}
