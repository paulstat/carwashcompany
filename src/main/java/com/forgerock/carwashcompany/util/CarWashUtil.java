package com.forgerock.carwashcompany.util;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Car Wash Utilities!
 * 
 * @author Paul Statham
 *
 */
public final class CarWashUtil {
    
    
    /**
     * Picks a random entry from a list of strings
     * 
     * @param quotes - strings to pick from
     * @return
     */
    public static String pickRandomQuote(List<String> quotes) {
        if (quotes == null || quotes.isEmpty()) {
            throw new IllegalArgumentException("String list cannot be null or empty!");
        }
        
        int startIndex = 0;
        int endIndex = quotes.size();

        int randIndex = ThreadLocalRandom.current().nextInt(startIndex, endIndex);

        return quotes.get(randIndex);
    }
    
    /**
     * Can't be instantiated
     */
    private CarWashUtil() {
        
    }

}
