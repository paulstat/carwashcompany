# FRock Car Wash Company
Welcome to the FRock Car Wash Company! This project demonstrates a simple worker type pattern, it employs a Producer, a concurrent blocking queue in the middle and a team of minions (consumers/workers) ready to wash cars!

This project is written in Java and requires JDK 1.8+ to build and run.

# Getting Started

### Build/run the project
To build and run the project perform the following steps

    git clone https://bitbucket.org/paulstat/carwashcompany.git
    cd carwashcompany/
    ./mvnw clean compile assembly:single
    cd target
    java -jar carwashcompany-0.0.1-SNAPSHOT-jar-with-dependencies.jar

### What you can expect to see

During the application startup the manager will initialise the minion workforce and log it to console

    15:04:11.083 [main] INFO com.forgerock.carwashcompany.ParallelCompanyManager - Initialising Parallel Minions!
    15:04:11.213 [pool-2-thread-1] INFO com.forgerock.carwashcompany.minion.Minion - Minion Bob ready for action!
    15:04:11.214 [pool-2-thread-4] INFO com.forgerock.carwashcompany.minion.Minion - Minion Phil ready for action!
    15:04:11.214 [pool-2-thread-5] INFO com.forgerock.carwashcompany.minion.Minion - Minion Stuart ready for action!
    15:04:11.215 [pool-2-thread-2] INFO com.forgerock.carwashcompany.minion.Minion - Minion Dave ready for action!
    15:04:11.215 [pool-2-thread-3] INFO com.forgerock.carwashcompany.minion.Minion - Minion Jerry ready for action!
    15:04:11.215 [pool-2-thread-6] INFO com.forgerock.carwashcompany.minion.Minion - Minion Tim ready for action!

As soon as the workforce is ready the customer manager will tell the Producer to start putting customers into the queue. The minion and customer greet each other and the minions start washing and chatting for a random amount of time

    15:05:14.241 [pool-1-thread-1] INFO com.forgerock.carwashcompany.CustomerProducer - Customer Dr Nefario arrived. Welcome! Please join the queue.
    15:05:14.244 [pool-1-thread-1] INFO com.forgerock.carwashcompany.CustomerProducer - Customer Dr Nefario has joined the queue.
    15:05:14.245 [pool-2-thread-4] INFO com.forgerock.carwashcompany.customer.CarWashCustomer - Dr Nefario: A dozen boogie robots. Boogie! Look at this! Watch me.
    15:05:14.245 [pool-2-thread-4] INFO com.forgerock.carwashcompany.minion.Minion - Phil: Ello I wash you now
    15:05:14.245 [pool-2-thread-4] INFO com.forgerock.carwashcompany.minion.Minion - Phil: Splosh
    15:05:16.245 [pool-1-thread-1] INFO com.forgerock.carwashcompany.CustomerProducer - Customer Felanious Gru arrived. Welcome! Please join the queue.
    15:05:16.245 [pool-1-thread-1] INFO com.forgerock.carwashcompany.CustomerProducer - Customer Felanious Gru has joined the queue.
    15:05:16.245 [pool-2-thread-5] INFO com.forgerock.carwashcompany.customer.CarWashCustomer - Felanious Gru: This is very important, you have to get the little girl a new unicorn toy.
    15:05:16.245 [pool-2-thread-5] INFO com.forgerock.carwashcompany.minion.Minion - Stuart: Ello I wash you now
    15:05:16.245 [pool-2-thread-5] INFO com.forgerock.carwashcompany.minion.Minion - Stuart: Scrub
    15:05:16.246 [pool-2-thread-4] INFO com.forgerock.carwashcompany.minion.Minion - Phil: Scrub

Once complete the minion says their goodbyes and moves on to the next customer in the queue

    15:11:57.543 [pool-2-thread-6] INFO com.forgerock.carwashcompany.minion.Minion - Tim: OK Margo, you done now, bye bye!

Occasionally the villain Vector will be served and kill (poison pill) the minion

    15:05:18.246 [pool-1-thread-1] INFO com.forgerock.carwashcompany.CustomerProducer - Customer Vector arrived. Welcome! Please join the queue.
    15:05:18.246 [pool-1-thread-1] INFO com.forgerock.carwashcompany.CustomerProducer - Customer Vector has joined the queue.
    15:05:18.246 [pool-2-thread-1] INFO com.forgerock.carwashcompany.customer.CarWashCustomer - Vector: Hahaha I go by the name of Vector. It's a mathematical term, represented by an arrow with both direction and magnitude. Vector! That's me, because I commit crimes with both direction and magnitude. Oh yeah! And now I will KILL YOU!
    15:05:18.246 [pool-2-thread-1] INFO com.forgerock.carwashcompany.minion.Minion - Bob: ARGH!!! BLEURGH {DIES}

No need to worry though as the CustomerManager is on the case and will periodically check to see if we need to respawn a minion(s) and allow that thread to run a task.

    15:11:56.531 [main] INFO com.forgerock.carwashcompany.CompanyManager - Hmmm our workforce is depleted, respawn!
    15:11:56.531 [pool-2-thread-1] INFO com.forgerock.carwashcompany.minion.Minion - Minion Tim ready for action!


### Tests

The project is by no means fully unit/integration tested but examples can be found in src/main/test demonstrating some of the concepts.
I have used JUnit 5

### Shortcomings

The project could be improved in a few ways

* Unit/integration tests need to be more complete
* Queue size is bounded to a static size of 10.
* The parallel worker threads are bounded to six.
* The list of worker classes from which the factory can instantiate is static, it's possible some sort of package scanner could be created to find new added Minion classes to avoid having to manually change this list.
* External configuration could be added to allow configuration of these instead of them being static compile time constants.
* No way to stop program without Ctrl-C for example. Could monitor specific keystroke which would allow elegant shutdown of worker threads.

### Scaling

As mentioned above the queue size and workforce are static, if the FRock carwash company needs to shrink and grow to meet its demands the following should be considered.

* The queue is bound to only 10 customers, any customers wanting to join the queue when it is full would be blocked from doing so until a space is free, it would be best to default this to a higher number and also make it configurable.
* The parallel work force is statically bound to a pool of 6 threads, it would be better to have a higher initial thread count.
* In addition to the above the size of the thread pool should be allowed to be dynamic with a configurable upper bound. This dynamic thread pool should grow and shrink to meet demand in order to not consume too many resources.

### Sequential/Parallel processing
By default the project will start in Parallel mode with a team of 6 minions serving their loyal despicable customers.

The minions aren't always that quick and will take a random amount of time to finish washing a car. To ensure that customers are seen to in sequence it
is possible to start the App in sequential mode as shown below. This employs a `java.util.concurrent.SynchronousQueue` where each insert operation must wait for a corresponding remove operation.

    java -Dsequential=true -jar carwashcompany-0.0.1-SNAPSHOT-jar-with-dependencies.jar

The above could've also been achieved by limiting the size of the thread pool to just 1.
